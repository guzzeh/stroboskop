# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git status
git commit -a -m "README"
git push
```

Naloga 6.2.3:
https://bitbucket.org/guzzeh/stroboskop/commits/114aed474faf1096c5722511896333f584b03492

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/guzzeh/stroboskop/commits/c6a68d782eb0d0b58c86649d747c05c0d2b3ff05

Naloga 6.3.2:
https://bitbucket.org/guzzeh/stroboskop/commits/36b4907558f52671ed538103983aacbe83b7f786

Naloga 6.3.3:
https://bitbucket.org/guzzeh/stroboskop/commits/70aeb8682b4365a31f4bcfedf73fcdad600a9cc8

Naloga 6.3.4:
https://bitbucket.org/guzzeh/stroboskop/commits/1f6afc6e7b37a0ab450e82e74e3a27e341ead249

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/guzzeh/stroboskop/commits/4e7d3dabbb9fc2120873e414cef23c46a4c81b90

Naloga 6.4.2:
https://bitbucket.org/guzzeh/stroboskop/commits/fd5af75af9987a7fb87886d8134e1d2661f642b4

Naloga 6.4.3:
https://bitbucket.org/guzzeh/stroboskop/commits/c1d435219bac4e4aa8820b558cf47db1b6d95d9c

Naloga 6.4.4:
https://bitbucket.org/guzzeh/stroboskop/commits/1f8a619547061fa581dd9f9b1267db51a7185dda